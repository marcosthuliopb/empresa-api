package br.com.ioasys.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import br.com.ioasys.DTO.EnterpriseDTO;
import br.com.ioasys.entity.Enterprise;

@Mapper(componentModel = "spring")
public interface EnterpriseMapper {
	 
	EnterpriseMapper INSTANCE = Mappers.getMapper( EnterpriseMapper.class ); 
 
	@Mapping(source = "enterpriseName", target = "enterprise_name")
    EnterpriseDTO enterpriseToEnterpriseDto(Enterprise enterprise); 
}
