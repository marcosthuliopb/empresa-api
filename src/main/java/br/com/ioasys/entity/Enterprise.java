package br.com.ioasys.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@javax.persistence.Entity
public class Enterprise {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="enterpriseName")
	private String enterpriseName;
	@Column(name="description")
	private String description;
	@Column(name="emailEnterprise")
	private String emailEnterprise;
	@Column(name="facebook")
	private String facebook;
	@Column(name="twitter")
	private String twitter;
	@Column(name="linkedin")
	private String linkedin;
	@Column(name="phone")
	private String phone;
	@Column(name="ownEnterprise")
	private Boolean ownEnterprise;
	@Column(name="photo")
	private String photo;
	@Column(name="value")
	private Integer value;
	@Column(name="shares")
	private Integer shares;
	@Column(name="sharePrice")
	private Double sharePrice;
	@Column(name="ownShares")
	private Integer ownShares;
	@Column(name="city")
	private String city;
	@Column(name="country")
	private String country;
	
	@OneToOne
	@JoinColumn(name = "type_id", referencedColumnName = "id")
	private EnterpriseType enterpriseType;

	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getEnterpriseName() {
		return enterpriseName;
	}


	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getEmailEnterprise() {
		return emailEnterprise;
	}


	public void setEmailEnterprise(String emailEnterprise) {
		this.emailEnterprise = emailEnterprise;
	}


	public String getFacebook() {
		return facebook;
	}


	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}


	public String getTwitter() {
		return twitter;
	}


	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}


	public String getLinkedin() {
		return linkedin;
	}


	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public Boolean getOwnEnterprise() {
		return ownEnterprise;
	}


	public void setOwnEnterprise(Boolean ownEnterprise) {
		this.ownEnterprise = ownEnterprise;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public Integer getValue() {
		return value;
	}


	public void setValue(Integer value) {
		this.value = value;
	}


	public Integer getShares() {
		return shares;
	}


	public void setShares(Integer shares) {
		this.shares = shares;
	}


	public Double getSharePrice() {
		return sharePrice;
	}


	public void setSharePrice(Double sharePrice) {
		this.sharePrice = sharePrice;
	}


	public Integer getOwnShares() {
		return ownShares;
	}


	public void setOwnShares(Integer ownShares) {
		this.ownShares = ownShares;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public EnterpriseType getEnterpriseType() {
		return enterpriseType;
	}


	public void setEnterpriseType(EnterpriseType enterpriseType) {
		this.enterpriseType = enterpriseType;
	}

	

	  
}
