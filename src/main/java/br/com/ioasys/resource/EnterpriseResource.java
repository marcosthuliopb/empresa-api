package br.com.ioasys.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ioasys.DTO.EnterpriseDTO;
import br.com.ioasys.entity.Enterprise;
import br.com.ioasys.service.EnterpriseService;

@RestController
@RequestMapping("/v1/enterprises")
public class EnterpriseResource {
	
	@Autowired
	public EnterpriseService enterpriseService;
	
	
	@GetMapping("/{id}")
	public ResponseEntity<?> retornarEnterprises(@PathVariable Integer id){
	
		Enterprise retorno = enterpriseService.pesquisarPorId(id);
		if(retorno!= null) {
			return ResponseEntity.ok(retorno);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping()
	public ResponseEntity<List<EnterpriseDTO>> pesquisarPorFiltro(@RequestParam Integer id, @RequestParam String nome) {
		List<EnterpriseDTO> retorno = enterpriseService.pesquisarPorFiltro(id, nome);
		if(retorno != null) {
			return ResponseEntity.ok(retorno);
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/all")
	public List<EnterpriseDTO> pesquisarTodos() {
		return enterpriseService.pesquisarTodos();
	}

}
