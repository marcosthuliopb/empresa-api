package br.com.ioasys.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.ioasys.entity.Enterprise;

@Repository
public interface EnterpriseRepository extends JpaRepository<Enterprise, Integer>{

	List<Enterprise> findById(Integer id);
	List<Enterprise> findByEnterpriseNameLike(String name);
	List<Enterprise> findByIdAndEnterpriseNameLike(Integer id, String name);
	
}
