package br.com.ioasys.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ioasys.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{

	Usuario findByEmail(String email);
	
}
