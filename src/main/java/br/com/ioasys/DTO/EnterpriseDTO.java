package br.com.ioasys.DTO;

import br.com.ioasys.entity.EnterpriseType;

public class EnterpriseDTO {
	
	private Integer id;

	private String enterprise_name;
	private String description;
	private String emailEnterprise;
	private String facebook;
	private String twitter;
	private String linkedin;
	private String phone;
	private Boolean ownEnterprise;
	private String photo;
	private Integer value;
	private Integer shares;
	private Double sharePrice;
	private Integer ownShares;
	private String city;
	private String country;
	
	private EnterpriseType enterpriseType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public String getEnterprise_name() {
		return enterprise_name;
	}

	public void setEnterprise_name(String enterprise_Name) {
		this.enterprise_name = enterprise_Name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmailEnterprise() {
		return emailEnterprise;
	}

	public void setEmailEnterprise(String emailEnterprise) {
		this.emailEnterprise = emailEnterprise;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Boolean getOwnEnterprise() {
		return ownEnterprise;
	}

	public void setOwnEnterprise(Boolean ownEnterprise) {
		this.ownEnterprise = ownEnterprise;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getShares() {
		return shares;
	}

	public void setShares(Integer shares) {
		this.shares = shares;
	}

	public Double getSharePrice() {
		return sharePrice;
	}

	public void setSharePrice(Double sharePrice) {
		this.sharePrice = sharePrice;
	}

	public Integer getOwnShares() {
		return ownShares;
	}

	public void setOwnShares(Integer ownShares) {
		this.ownShares = ownShares;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public EnterpriseType getEnterpriseType() {
		return enterpriseType;
	}

	public void setEnterpriseType(EnterpriseType enterpriseType) {
		this.enterpriseType = enterpriseType;
	}

	
}
