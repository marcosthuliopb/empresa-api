package br.com.ioasys.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.ioasys.DTO.EnterpriseDTO;
import br.com.ioasys.entity.Enterprise;
import br.com.ioasys.mapper.EnterpriseMapper;
import br.com.ioasys.repository.EnterpriseRepository;

@Service
public class EnterpriseService {

	@Autowired
	public EnterpriseRepository enterpriseRepository;
	// @Autowired
	// EnterpriseMapper map;

	public List<EnterpriseDTO> pesquisarPorFiltro(Integer id, String name) {
		List<Enterprise> list = new ArrayList<>();
		if (!StringUtils.isEmpty(name) && id != null) {
			list = enterpriseRepository.findByIdAndEnterpriseNameLike(id, name);
		} else if (!StringUtils.isEmpty(name)) {
			list = enterpriseRepository.findByEnterpriseNameLike(name);
		} else if (id != null) {
			list = enterpriseRepository.findById(id);
		}
		
		List<EnterpriseDTO> listDto = new ArrayList<EnterpriseDTO>();
		list.forEach(e -> {
			listDto.add(EnterpriseMapper.INSTANCE.enterpriseToEnterpriseDto(e));
		});
		return listDto;
	}

	public List<EnterpriseDTO> pesquisarTodos() {

		List<Enterprise> list = enterpriseRepository.findAll();
		List<EnterpriseDTO> listDto = new ArrayList<EnterpriseDTO>();
		list.forEach(e -> {
			listDto.add(EnterpriseMapper.INSTANCE.enterpriseToEnterpriseDto(e));
		});
		return listDto;
	}

	public Enterprise pesquisarPorId(Integer id) {
		Enterprise retorno = null;
		try {
			retorno = enterpriseRepository.findOne(id);
		} catch (Exception e) {
			return null;
		}

		return retorno;
	}

}
