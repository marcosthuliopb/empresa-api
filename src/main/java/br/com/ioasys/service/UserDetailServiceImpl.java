//package br.com.ioasys.service;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import br.com.ioasys.config.UserSS;
//import br.com.ioasys.entity.Usuario;
//import br.com.ioasys.repository.UsuarioRepository;
//
//@Service
//public class UserDetailServiceImpl implements UserDetailsService{
//
//	
//	@Autowired
//	UsuarioRepository usuarioRepository;
//	
//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		
//		Usuario user = usuarioRepository.findByEmail(username);
//		 List<String> lista = new ArrayList<String>();
//		    lista.add("ROLE_ADMIN");
//		
//		if(user == null) {
//			throw new UsernameNotFoundException(username);
//		}
//		return new UserSS(user.getId(),user.getEmail(), user.getSenha(), (Set<String>) lista);
//	}
//
//}
