package br.com.ioasys.mapper;

import br.com.ioasys.DTO.EnterpriseDTO;
import br.com.ioasys.entity.Enterprise;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-11-02T22:09:50-0300",
    comments = "version: 1.4.1.Final, compiler: Eclipse JDT (IDE) 3.19.0.v20190903-0936, environment: Java 1.8.0_251 (Oracle Corporation)"
)
@Component
public class EnterpriseMapperImpl implements EnterpriseMapper {

    @Override
    public EnterpriseDTO enterpriseToEnterpriseDto(Enterprise enterprise) {
        if ( enterprise == null ) {
            return null;
        }

        EnterpriseDTO enterpriseDTO = new EnterpriseDTO();

        enterpriseDTO.setEnterprise_name( enterprise.getEnterpriseName() );
        enterpriseDTO.setId( enterprise.getId() );
        enterpriseDTO.setDescription( enterprise.getDescription() );
        enterpriseDTO.setEmailEnterprise( enterprise.getEmailEnterprise() );
        enterpriseDTO.setFacebook( enterprise.getFacebook() );
        enterpriseDTO.setTwitter( enterprise.getTwitter() );
        enterpriseDTO.setLinkedin( enterprise.getLinkedin() );
        enterpriseDTO.setPhone( enterprise.getPhone() );
        enterpriseDTO.setOwnEnterprise( enterprise.getOwnEnterprise() );
        enterpriseDTO.setPhoto( enterprise.getPhoto() );
        enterpriseDTO.setValue( enterprise.getValue() );
        enterpriseDTO.setShares( enterprise.getShares() );
        enterpriseDTO.setSharePrice( enterprise.getSharePrice() );
        enterpriseDTO.setOwnShares( enterprise.getOwnShares() );
        enterpriseDTO.setCity( enterprise.getCity() );
        enterpriseDTO.setCountry( enterprise.getCountry() );
        enterpriseDTO.setEnterpriseType( enterprise.getEnterpriseType() );

        return enterpriseDTO;
    }
}
